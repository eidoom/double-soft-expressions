#!/usr/bin/env python3

import sympy
import platform

print(
    f"Using: \n  * Python {platform.python_version()}\n  * Sympy {sympy.__version__}\n"
)

sympy.init_printing()

p = sympy.IndexedBase("p")
e = sympy.IndexedBase("e")
sA = sympy.IndexedBase("sA")
sB = sympy.IndexedBase("sB")
sBA = sympy.IndexedBase("sBA")
sAB = sympy.IndexedBase("sAB")
sAA = sympy.IndexedBase("sAA")
sBB = sympy.IndexedBase("sBB")

i1, q1, q2, r1, r2, h1, h2, mu1, mu2, pl, mi = sympy.symbols(
    "i1, q1, q2, r1, r2, h1, h2, mu1, mu2, pl, mi", cls=sympy.Idx
)

i, q, r, mu = map(sympy.Wild, ["i", "q", "r", "mu"])

term0 = (p[i1, mu1] * e[h1, q1, r1, mu1]) * (p[q1, mu2] * e[h2, q2, r2, mu2]) - (
    p[i1, mu1] * e[h2, q2, r2, mu1]
) * (p[q2, mu2] * e[h1, q1, r1, mu2])
print(term0)
print()

pp = {h1: pl, h2: pl}
term_pp = term0.subs(pp)
print(term_pp)
print()

rep_p = (e[pl, q, r, mu], sAB[r, mu, q] / (sympy.sqrt(2) * sAA[r, q]))
term_pp_1 = term_pp.replace(*rep_p)
print(term_pp_1)
print()

rep_pe = (sAB[r, mu, q] * p[i, mu], sAB[r, i, q])
term_pp_2 = term_pp_1.replace(*rep_pe)
print(term_pp_2)
print()
